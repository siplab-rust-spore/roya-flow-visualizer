#!/usr/bin/env python3

import argparse
import glob
from itertools import product

import numpy as np
from flowiz import read_flow, convert_from_flow
import matplotlib.pyplot as plt

TAG_FLOAT = 202021.25

START_POINT = np.array([73, 550])
TOLERANCE = 0.1

SPORE_MASK = 2

WIDTH = 640
HEIGHT = 480

COLOR_LIST = [
    "blue",
    "orange",
    "green",
    "red",
    "purple",
    "brown",
    "pink",
    "gray",
    "olive",
    "cyan",
]


def get_next_point(current_point, flow):
    vector_x, vector_y = flow[round(current_point[0]), round(current_point[1])]

    next_point_y = current_point[0] + vector_y
    next_point_x = current_point[1] + vector_x
    next_point = np.array([next_point_y, next_point_x])

    if np.sum(np.sqrt(np.power(next_point - current_point, 2))) >= TOLERANCE:
        if (
            (0 <= round(next_point[0]))
            and (round(next_point[0]) < HEIGHT)
            and (0 <= round(next_point[1]))
            and ((next_point[1]) < WIDTH)
        ):
            return (next_point[0], next_point[1])

        return None
    else:
        return None


def visualizer(path: str):
    flo_files = glob.glob(path + "/*.flo")
    segm_files = glob.glob(path + "/*.png")

    flo_files.sort()
    segm_files.sort()

    width = 640
    height = 480
    step = 25

    point_paths_list = [
        [(x[0], x[1])]
        for x in product(
            list(range(0, height - 1, int(height / step))),
            list(range(0, width - 1, int(width / step))),
        )
    ]

    for flo_file, segm_file in zip(flo_files, segm_files):
        flow = read_flow(flo_file)
        flow *= 8

        for point_path in point_paths_list:

            # Get the latest point
            current_point = point_path[-1]

            next_point = get_next_point(current_point, flow)

            if next_point is not None:
                point_path.append(next_point)

        for point_path in point_paths_list:
            color = COLOR_LIST[point_paths_list.index(point_path) % len(COLOR_LIST)]

            for first_point, second_point in zip(point_path, point_path[1:]):
                first_y, first_x = first_point
                second_y, second_x = second_point
                first_y, first_x = int(first_y), int(first_x)
                second_y, second_x = int(second_y), int(second_x)

                plt.plot([first_x, second_x], [first_y, second_y], color=color)

        color_flow = convert_from_flow(flow)
        plt.imshow(color_flow)

        print(flo_file)
        plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Display path for optical flow")
    parser.add_argument("--path", required=True)

    args = parser.parse_args()
    visualizer(args.path)
